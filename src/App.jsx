import { useState } from 'react'
import quoteDB from './db/quotes.json'
import colors from './db/colors.js'
import QuoteBox from './components/QuoteBox'

import './App.css'

function App() {
  const ramdomArray=(array)=>{
    return array[Math.floor(array.length*Math.random()) ]
  }
  const [quote, setQuote] = useState(ramdomArray(quoteDB))
  const [color, setColor] = useState(ramdomArray(colors))
  const [width, setWidth] = useState('80%')

  const changeQuote=()=>{
    setQuote(ramdomArray(quoteDB))
    setColor(ramdomArray(colors))

    if (quote.quote.length*16>screen.width-(screen.width*0.3)){
      setWidth('60%')
    }else{
      setWidth(quote.quote.length+'%')
    }
  }
  const backgroundObject={
    backgroundColor:color
  }
  return (
    <div className="app" style={{'backgroundColor':color}} >
      <QuoteBox color={color} quote={quote} changequote={changeQuote} width={width}></QuoteBox>
    </div>
  )
}

export default App
