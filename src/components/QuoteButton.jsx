import { render } from "react-dom"

const QuoteButton=({color,changequote})=>{
    return(
        <button className="boton" onClick={changequote} style={{'backgroundColor':color}}>
            <i className="fa-solid fa-chevron-right"></i>
        </button>
    )
}
export default QuoteButton