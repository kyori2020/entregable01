import QuoteButton from './QuoteButton'

const QuoteBox=({quote,color,changequote,width})=>{

    return (
        <article className='quote' style={{'color':color,'width':width}}>
            <div>
                <i className="fa-solid fa-quote-left"></i>
                <p >{quote.quote}</p>
            </div>
            <h4>{quote.author}</h4>
            <QuoteButton color={color} changequote={changequote}/>
        </article>
    )
}
export default QuoteBox